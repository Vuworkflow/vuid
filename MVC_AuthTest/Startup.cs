﻿using Owin;
using VuidClient;
using VuidClient.Auth;
using VuidClient.Interfaces;

namespace MVCAuthTest
{
    public sealed class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            IVuidAuthConfig config = new VuidAuthConfig()
            {
                Authority = "https://localhost:44393/identity",
                ClientId = "mvc",
                Scope = "openid profile roles",
                RedirectUri = "http://localhost:57782/"
            };

            AuthClient.Register(app, config);
        }
    }
}