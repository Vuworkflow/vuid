﻿using System.Collections.Generic;
using VuidClient.Interfaces;

namespace VuidClient.Auth
{
    public class VuidAuthConfig : IVuidAuthConfig
    {
        public string Authority { get; set; }
        public string ClientId { get; set; }
        public string Scope { get; set; }
        public string RedirectUri { get; set; }
        public Dictionary<string, string> AppSpecifcData { get; set; }
    }
}