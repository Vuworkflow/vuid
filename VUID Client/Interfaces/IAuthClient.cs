﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Owin;

namespace VuidClient.Interfaces
{
    public interface IAuthClient
    {
        void Register(IAppBuilder app, IVuidAuthConfig config);
    }
}