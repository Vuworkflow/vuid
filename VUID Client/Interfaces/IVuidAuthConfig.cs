﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VuidClient.Interfaces
{
    public interface IVuidAuthConfig
    {
        string Authority { get; set; }
        string ClientId { get; set; }
        string Scope { get; set; }
        string RedirectUri { get; set; }
        Dictionary<string, string> AppSpecifcData { get; set; }
    }
}